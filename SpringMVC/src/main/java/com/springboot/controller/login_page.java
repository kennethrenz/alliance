package com.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class login_page {
	
	@RequestMapping("/login")
	public String loadLogin(ModelMap map){
		return "login_page";
		
	}

}
